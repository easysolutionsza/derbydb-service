package com.transportsystem.derbyDB.api.web.rest;

import com.transportsystem.derbyDB.api.domain.RouteDetail;
import com.transportsystem.derbyDB.api.persistence.entity.Planet;
import com.transportsystem.derbyDB.api.service.TransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/api",
        produces = {
                APPLICATION_JSON_VALUE,
        })
public class TransportController {

    private TransportService transportService;

    @Autowired
    public TransportController(final TransportService transportService)
    {
        this.transportService = transportService;
    }


    @RequestMapping(value = "/test", method = GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String test()
    {
        return "all good";
    }

    @RequestMapping(value = "/planets", method = GET)
    public List<Planet>getPlanets()
    {
        return transportService.listAllPlanet();
    }

    @RequestMapping(value = "/details", method = GET)
    public List<RouteDetail>getDetails()
    {
        return transportService.routeDetails();
    }

}
