package com.transportsystem.derbyDB.api.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RouteDetail {

    private int route_id;
    private String planet_origin;
    private String planet_destination;
    private Double distance;
    private Double traffic_delay;

}
